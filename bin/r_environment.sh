#!/bin/bash
GCC_VERSION="4.3"
export R_INCLUDE="/app/vendor/R/lib64/R/include"
export PATH="/app/vendor/R/bin:/app/vendor/.apt/usr/bin:/app/bin:/usr/ruby1.9.2/bin:/usr/local/bin:/usr/local/sbin:/usr/bin:/bin:/usr/sbin:/sbin:$PATH"
export LD_LIBRARY_PATH="/app/jre/lib/amd64/server:/app/vendor/R/lib/R/library/rJava/jri:/app/vendor/.apt/usr/lib/libblas:/app/vendor/.apt/usr/lib/lapack:/app/vendor/.apt/usr/lib/x86_64-linux-gnu:/app/vendor/.apt/usr/lib/i386-linux-gnu:/app/vendor/.apt/usr/lib:$LD_LIBRARY_PATH"
export LIBRARY_PATH="/app/vendor/.apt/usr/lib/x86_64-linux-gnu:/app/vendor/.apt/usr/lib/i386-linux-gnu:/app/vendor/.apt/usr/lib:$LIBRARY_PATH"
export INCLUDE_PATH="/app/vendor/.apt/usr/include:$INCLUDE_PATH"
export CPATH="$INCLUDE_PATH"
export CPPPATH="$INCLUDE_PATH"
export PKG_CONFIG_PATH="/app/vendor/.apt/usr/lib/x86_64-linux-gnu/pkgconfig:/app/vendor/.apt/usr/lib/i386-linux-gnu/pkgconfig:/app/vendor/.apt/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
export LDFLAGS="-L/app/vendor/.apt/usr/lib/libblas -L/app/vendor/.apt/usr/lib/lapack $LDFLAGS"
export JAVA_HOME="/app"

export R_BASE=/app/vendor/R
export R_HOME=$R_BASE/lib64/R
export JAVA_CPPFLAGS="-I$JAVA_HOME/include -I$JAVA_HOME/include/linux"
export R_INCLUDE=$R_HOME/include
export PATH=$JAVA_HOME/bin:$R_BASE/bin:/app/vendor/gcc-$GCC_VERSION/bin:$PATH


R CMD javareconf -e